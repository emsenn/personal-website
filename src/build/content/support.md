+++
title = "Support"
author = ["emsenn"]
draft = false
[menu.index]
  weight = 3003
  identifier = "support"
+++

I release as much of my work as I can into the _public domain_, so
that anyone can use and change it however serves them best. That is
made possible with financial contributions from humans like you. Thank
you!

I currently accept support through the following platforms:

-   One-time through...
    -   [Paypal](https://www.paypal.me/emsenn)
    -   [Ko-fi](https://ko-fi.com/emsenn)
-   Recurring through...
    -   [Liberapay](https://liberapay.com/emsenn/donate)

If there is another service through which you'd like to contribute,
please [send an email](mailto:emsenn@emsenn.net). **_Please note,_** as my work is released into the
public domain, financial contributions do not provide access to any
"exclusive" content.
