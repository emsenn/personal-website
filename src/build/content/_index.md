+++
title = "My name is emsenn"
author = ["emsenn"]
draft = false
+++

I'm a **writer** and **software** developer with a focus on the
intersection of the two. This website is a collection of information
about me as a person.
