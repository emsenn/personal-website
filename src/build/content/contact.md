+++
title = "Contact"
author = ["emsenn"]
draft = false
[menu.index]
  weight = 3002
  identifier = "contact"
+++

**_In a hurry?_** Send an email to [emsenn.net](mailto:emsenn@emsenn.net).


## Overview {#overview}

This website is just one part of my representation online, and doesn't
itself provide a means for people to communicate back with me. Below
is a list of other digital representations I maintain. Interested in
all my accounts? See ["Public Accounts"](_public-accounts_).


## Email {#email}

You can **_send an email_** to either me, privately or to my "public
inbox." My private email is [emsenn@emsenn.net](mailto:emsenn@emsenn.net).

But I prefer to receive messages through my "public inbox," in case
whatever you want to say is something others would like to read my
response to. And if you're asking a question about a piece of software
I've made, you're more likely to get a quick response.


## Microblogging {#microblogging}

I'm active within the _Fediverse_, a network of communication
platforms. My main account is at [@emsenn@tenforward.social](https://tenforward.social/@emsenn).
